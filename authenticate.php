<?php
    session_start();
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "justip";

    $connect = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    
    $query = "SELECT id, password, nama_depan, nama_belakang, alamat, kota, provinsi, kode_pos, no_hp FROM user WHERE email = ?";
    if(isset($_POST['create'])){
        $query = "INSERT into user(email,password) values('$_POST[email]','$_POST[password]')";
        mysqli_query($connect,$query);
        header('Location: index.php');
    }else if ($stmt = $connect->prepare($query)){
        $stmt->bind_param("s", $_POST['email']);
        $stmt->execute();
        $stmt->store_result();
       
        if ($stmt->num_rows > 0) {
            $stmt->bind_result($id, $password, $nama_depan, $nama_belakang, $alamat, $kota, $provinsi, $kode_postal, $no_hp);
            $stmt->fetch();

            if ($_POST['password'] === $password) {
                session_regenerate_id();
                $_SESSION['logged_in'] = TRUE;
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['nama_depan'] = $nama_depan;
                $_SESSION['nama_belakang'] = $nama_belakang;
                $_SESSION['alamat'] = $alamat;
                $_SESSION['kota'] = $kota;
                $_SESSION['provinsi'] = $provinsi;
                $_SESSION['kode_postal'] = $kode_postal;
                $_SESSION['no_hp'] = $no_hp;
                $_SESSION['id'] = $id;
                header('Location: index.php');
            } else {
                $_SESSION['pwmessage'] = 'Incorrect password';
            }            
        } else {
            $_SESSION['usermessage'] = 'Incorrect username';
        }
	    $stmt->close();
    }
    
?>