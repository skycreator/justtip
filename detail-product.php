<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Product Detail</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="global_assets/js/main/jquery.min.js"></script>
	<script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="global_assets/js/plugins/ui/slinky.min.js"></script>
	<script src="global_assets/js/plugins/ui/fab.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script src="assets/js/app.js"></script>
	<script src="global_assets/js/demo_pages/dashboard.js"></script>
	<!-- /theme JS files -->

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->
      
  
  
      <style>
  
  /*****************globals*************/
  body {
    font-family: 'open sans';
    overflow-x: hidden; }
  
  img {
    max-width: 100%; }
  
  .preview {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
        -ms-flex-direction: column;
            flex-direction: column; }
    @media screen and (max-width: 996px) {
      .preview {
        margin-bottom: 20px; } }
  
  .preview-pic {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
            flex-grow: 1; }
  
  .preview-thumbnail.nav-tabs {
    border: none;
    margin-top: 15px; }
    .preview-thumbnail.nav-tabs li {
      width: 18%;
      margin-right: 2.5%; }
      .preview-thumbnail.nav-tabs li img {
        max-width: 100%;
        display: block; }
      .preview-thumbnail.nav-tabs li a {
        padding: 0;
        margin: 0; }
      .preview-thumbnail.nav-tabs li:last-of-type {
        margin-right: 0; }
  
  .tab-content {
    overflow: hidden; }
    .tab-content img {
      width: 95%;
      -webkit-animation-name: opacity;
              animation-name: opacity;
      -webkit-animation-duration: .3s;
              animation-duration: .3s; }
  
  .card {
    margin-top: 50px;
    background: #eee;
    padding: 3em;
    line-height: 1.5em; }
  
  @media screen and (min-width: 997px) {
    .wrapper {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex; } }
  
  .details {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
        -ms-flex-direction: column;
            flex-direction: column; }
  
  .colors {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
            flex-grow: 1; }
  
  .product-title, .price, .sizes, .colors {
    text-transform: UPPERCASE;
    font-weight: bold; }
  
  .checked, .price span {
    color: #ff9f1a; }
  
  .product-title, .rating, .product-description, .price, .vote, .sizes {
    margin-bottom: 15px; }
  
  .product-title {
    margin-top: 0; }
  
  .size {
    margin-right: 10px; }
    .size:first-of-type {
      margin-left: 40px; }
  
  .color {
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
    height: 2em;
    width: 2em;
    border-radius: 2px; }
    .color:first-of-type {
      margin-left: 20px; }
  
  .add-to-cart, .like {
    background: #ff9f1a;
    padding: 1.2em 1.5em;
    border: none;
    text-transform: UPPERCASE;
    font-weight: bold;
    color: #fff;
    -webkit-transition: background .3s ease;
            transition: background .3s ease; }
    .add-to-cart:hover, .like:hover {
      background: #b36800;
      color: #fff; }
  
  .not-available {
    text-align: center;
    line-height: 2em; }
    .not-available:before {
      font-family: fontawesome;
      content: "\f00d";
      color: #fff; }
  
  .orange {
    background: #ff9f1a; }
  
  .green {
    background: #85ad00; }
  
  .blue {
    background: #0076ad; }
  
  .tooltip-inner {
    padding: 1.3em; }
  
  @-webkit-keyframes opacity {
    0% {
      opacity: 0;
      -webkit-transform: scale(3);
              transform: scale(3); }
    100% {
      opacity: 1;
      -webkit-transform: scale(1);
              transform: scale(1); } }
  
  @keyframes opacity {
    0% {
      opacity: 0;
      -webkit-transform: scale(3);
              transform: scale(3); }
    100% {
      opacity: 1;
      -webkit-transform: scale(1);
              transform: scale(1); } }
  
  /*# sourceMappingURL=style.css.map */
      </style>

</head>

<body>

	<!-- Page header -->
	<div class="page-header page-header-dark">

		<!-- Main navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-transparent">
			
			<div class="d-md-none">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
					<i class="icon-tree5"></i>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="navbar-mobile">
				<ul class="navbar-nav ml-md-auto">

					<li class="nav-item dropdown dropdown-user">
						<?php 
							
							if(isset($_SESSION['user'])) {
						?>
							<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
								<img src="global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" alt="">
								<span>Welcome, <?=$_SESSION['nama_depan']?></span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<a href="profile.php" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
								<div class="dropdown-divider"></div>
									<a href="signout.php" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
								</div>
						<?php		
							} else {
						?>
							<a href="signin.php" class="navbar-nav-link">
								<span>Already logged in ?</span>
							</a>
						<?php		
							}
						?>
				</ul>
			</div>
		</div>
		<!-- /main navbar -->


		<!-- Page header content -->
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4>Detail Produk</h4>
				<a href="#" class="header-elements-toggle text-white d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none bg-transparent py-0 border-0 mb-3 mb-md-0">
				<form action="#">
					<div class="form-group form-group-feedback form-group-feedback-right">
						<input type="search" class="form-control bg-light-alpha border-transparent wmin-md-200" placeholder="Search">
						<div class="form-control-feedback">
							<i class="icon-search4 font-size-sm"></i>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- /page header content -->


		<!-- Secondary navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-top-0">
			<div class="d-md-none w-100">
				<button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse" data-target="#navbar-navigation">
					<i class="icon-menu-open mr-2"></i>
					Main navigation
				</button>
			</div>

			<div class="navbar-collapse collapse" id="navbar-navigation">
				<ul class="navbar-nav navbar-nav-highlight">
					<li class="nav-item">
						<a href="index.html" class="navbar-nav-link">
							<i class="icon-home4 mr-2"></i>
							Go to Home
						</a>
					</li>		
				</ul>
			</div>
		</div>
		<!-- /secondary navbar -->
	</div>
	<!-- /page header -->
		

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
				<!-- Dashboard content -->
				<div class="row">
					<div class="col-xl-12">
<?php
	include_once("config.php");
	$detail = mysqli_query($conn, "SELECT * FROM barang where id_barang=$_GET[id]");
	$b=mysqli_fetch_array($detail);
?>

						<!-- Marketing campaigns -->
						<div class="card">
							<div class="card-header header-elements-sm-inline">
								<!-- <h6 class="card-title"><?= $b['nama_barang']?></h6> -->
								<div class="header-elements">
			          </div>
							</div>
							<div class="table">
								<table class="table">
									<tbody>
										<tr>
											<td rowspan="2">
													<div class="d-flex align-items-center">
															<img src="image/<?=$b['photo_barang']?>" class="rectangle" width="auto" height="150" alt="">
													</div>
											</td>
											<td width="500" class="table-inbox-message">
													<div class="details">
															<h3 class="product-title"><?= $b['nama_barang']?></h3>
													</div>
											</td>
										</tr>
										<tr>
											<td>
													<p class="product-description">
														<p><?= $b['desc_barang'];?> 
														</p>
												</p>
											</td>
										</tr>
									</tbody>
								</table>
								<br><br><br>
								<ul style="text-align:center; padding-left:unset;">
											<?php
											$s = mysqli_query($conn, "SELECT email,no_hp FROM user where id=$b[id_user]");
											$de = mysqli_fetch_array($s);
											?>
										<li style="display:inline; margin: 10px">
												<a target="_blank" href="mailto:<?= $de['email'];?>" class="btn btn-light rounded-round btn-icon btn-float" style="width:auto">
													<i class="icon-envelope" style="color: darkcyan"></i>
												</a>
										</li>
										<li style="display:inline; margin: 10px">
												<a target="_blank" href="https://wa.me/<?= str_replace("+", "", $de['no_hp'])?>" class="btn btn-light rounded-round btn-icon btn-float" style="width:auto; height: auto; ">
													<img src="whatsapp.png" style="width: 16px;">
												</a>
										</li>
								</ul> 
								<br><br>
								<?php}?>
							</div>
						</div>
						<!-- /marketing campaigns -->
					</div>
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


	<!-- Footer -->
	<div class="navbar navbar-expand-lg navbar-light">
		<div class="text-center d-lg-none w-100">
			<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
				<i class="icon-unfold mr-2"></i>
				Footer
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-footer">
			<span class="navbar-text">
				&copy; 2019. <a href="#">Justtip</a> by <a href="#" target="_blank">Pentol Team</a>
			</span>
		</div>
	</div>
	<!-- /footer -->
		
</body>
</html>
