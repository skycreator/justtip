<?php
// include database connection file
include_once("config.php");

// Check if form is submitted for user update, then redirect to homepage after update
if(isset($_POST['update']))
{   
    $id = $_POST['id_barang'];

    $nama_barang = $_POST['nama_barang'];
    $desc_barang = $_POST['desc_barang'];
    $status_barang = $_POST['status_barang'];

    // update user data
    $result = mysqli_query($conn, "UPDATE `barang` SET nama_barang='$nama_barang',desc_barang='$desc_barang',status_barang='$status_barang' WHERE id_barang='$id'");

    // Redirect to homepage to display updated user in list
    header("Location: index.php");
}
?>
<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];

// Fetech user data based on id
$result = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang=$id");

while($barang2 = mysqli_fetch_array($result))
{
    $nama_barang = $barang2['nama_barang'];
    $desc_barang = $barang2['desc_barang'];
    $status_barang = $barang2['status_barang'];
}
?>
<html>
<head>  
    <title>Edit User Data</title>
</head>

<body>
    <a href="index.php">Home</a>
    <br/><br/>

    <form name="update_user" method="post" action="edit.php">
        <table border="0">
            <tr> 
                <td>Name</td>
                <td><input type="text" name="nama_barang" value=<?php echo $nama_barang;?>></td>
            </tr>
            <tr> 
                <td>Email</td>
                <td><input type="text" name="desc_barang" value=<?php echo $desc_barang;?>></td>
            </tr>
            <tr> 
                <td>Status</td>
                <td><input type="text" name="status_barang" value=<?php echo $status_barang;?>></td>
            </tr>
            <tr>
                <td><input type="hidden" name="id_barang" value=<?php echo $_GET['id'];?>></td>
                <td><input type="submit" name="update" value="Update"></td>
            </tr>
        </table>
    </form>
</body>
</html>