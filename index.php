<?php
	session_start();
	include_once("./get_barang.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Justtip</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/popup_post.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="global_assets/js/main/jquery.min.js"></script>
	<script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="global_assets/js/plugins/ui/slinky.min.js"></script>
	<script src="global_assets/js/plugins/ui/fab.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script src="assets/js/app.js"></script>
	<script src="global_assets/js/demo_pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Page header -->
	<div class="page-header page-header-dark">

		<!-- Main navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-transparent">
		
			<div class="d-md-none">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
					<i class="icon-tree5"></i>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="navbar-mobile">
				<ul class="navbar-nav ml-md-auto">
					<li class="nav-item dropdown dropdown-user">
						<?php 
							
							if(isset($_SESSION['user'])) {
						?>
							<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
								<img src="global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" alt="">
								<span>Welcome, <?=$_SESSION['nama_depan']?></span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<a href="profile.php" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
								<div class="dropdown-divider"></div>
									<a href="signout.php" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
								</div>
						<?php		
							} else {
						?>
							<a href="signin.php" class="navbar-nav-link">
								<span>Already logged in ?</span>
							</a>
						<?php		
							}
						?>
						

						
					</li>
				</ul>
			</div>
		</div>
		<!-- /main navbar -->


		<!-- Page header content -->
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<?php
				?>
				<h4>Home</h4>
				<a href="#" class="header-elements-toggle text-white d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none bg-transparent py-0 border-0 mb-3 mb-md-0">
				<form action="#">
					<div class="form-group form-group-feedback form-group-feedback-right">
						<input type="search" class="form-control bg-light-alpha border-transparent wmin-md-200" placeholder="Search">
						<div class="form-control-feedback">
							<i class="icon-search4 font-size-sm"></i>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- /page header content -->


		<!-- Secondary navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-top-0">
			<div class="d-md-none w-100">
				<button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse" data-target="#navbar-navigation">
					<i class="icon-menu-open mr-2"></i>
					Main navigation
				</button>
			</div>

			<div class="navbar-collapse collapse" id="navbar-navigation">
				<ul class="navbar-nav navbar-nav-highlight">
					<li class="nav-item">
						<a href="index.php" class="navbar-nav-link active">
							<i class="icon-home4 mr-2"></i>
							Home
						</a>
					</li>		
				</ul>
			</div>
		</div>
		<!-- /secondary navbar -->


		<!-- Floating menu -->
		<ul class="fab-menu fab-menu-absolute fab-menu-top-right" data-fab-toggle="click">
			<li>
				<a class="fab-menu-btn btn bg-pink-300 btn-float rounded-round btn-icon">
					<i class="fab-icon-open icon-plus3"></i>
					<i class="fab-icon-close icon-cross2"></i>
				</a>

				<ul class="fab-menu-inner">
					<li>
						<div data-fab-label="Post Item">
							<button class="btn btn-light rounded-round btn-icon btn-float"
								 onclick="document.getElementById('id01').style.display='block'" 
								style="width:auto">
								<i class="icon-pencil"></i>
							</button>
						</div>
					</li>				
				</ul>
			</li>
		</ul>
		<!-- /floating menu -->

	</div>
	<!-- /page header -->
		
	<!-- /pop up-->
	<div id="id01" class="modal">
		<span onclick="document.getElementById('id01').style.display='none'" \
			class="close">x</span>
		<form class="modal-content animate" enctype="multipart/form-data" action="./add.php" method="post">	
			<div class="container">
				<label><b>Nama Barang</b></label>
				<input type="text" placeholder="Nama Barang" name="barang" required>

				<label><b>Detail Barang</b></label>
				<input type="text" placeholder="Detail Barang" name="detail" required>

				<label><b>Reward</b></label>
				<input type="text" placeholder="Reward" name="reward" required>

				<label><b>Photo</b></label>
				<input type="file" placeholder="Photo" name="photo" required>

				<!-- <label><b>Contoh Foto Gambar</b></label> -->
				<!-- <div class="clearfix"> 
					<button type="submit" class="signupbtn">Submit</button> 
				</div> -->
				<div class="clearfix"> 
					<button type="submit" name="Submit" class="signupbtn" value="add">Submit</button> 
				</div>  
			</div>
		</form>
	</div>

	<!-- /pop up-->

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
				<!-- Dashboard content -->
				<div class="row">
					<div class="col-xl-12">

						<!-- Marketing campaigns -->
						<div class="card">
							<div class="card-header header-elements-sm-inline">
								<h6 class="card-title">Entrusted List</h6>
<!-- 								<div class="header-elements">
									<div class="list-icons ml-3">
				                		<div class="list-icons-item dropdown">
				                			<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
											<div class="dropdown-menu">
												<a href="#" class="dropdown-item"><i class="icon-filter3"></i> Filter</a>
												<a href="#" class="dropdown-item"><i class="icon-sync"></i> Update</a>
											</div>
				                		</div>
				                	</div>
			                	</div>
 -->							</div>
							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th>Picture</th>
											<th>Title</th>
											<th>Fee</th>
											<th>Entrusted</th>
											<th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
										</tr>
									</thead>
									<tbody>
<!-- 										<tr>
											<td>
												<div class="d-flex align-items-center">
													<a href="#">
														<img src="global_assets/images/brands/facebook.png" class="rectangle" width="150" height="150" alt="">
													</a>
												</div>
											</td>
											<td  class="table-inbox-message">
												<a href="detail-product.php"><h6 class="font-weight-semibold">Kemeja Korea</h6></a>
												<span class="text-muted font-weight-normal" style="padding: top -100cp;">Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet</span>
											</td>
											<td><h6 class="font-weight-semibold">Rp 5,489</h6></td>
											<td>
												<a href="#">
													<div class="text-center">
														<img src="global_assets/images/brands/facebook.png" class="rounded-circle" width="35" height="35">
													</div>
													<h6 class="font-weight-semibold text-center">Samsul</h6>
												</a>
											</td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="edit.php?id=<?php echo $barang['id_barang']?>" class="dropdown-item" onclick="document.getElementById('id01').style.display='block'" ><i class="icon-pencil"></i> Edit Post</a>
															<a href="delete.php?id=<?php echo $barang['id_barang'] ?>" class="dropdown-item"><i class="icon-cross2"></i> Delete Post</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
 -->										<?php  

											while($barang = mysqli_fetch_array($result)) {
												echo "<tr>";
												echo "<td><img width=200 src='image/".$barang['photo_barang']."'></td>";
												echo "<td><a href='detail-product.php?id=$barang[id_barang]' >".$barang['nama_barang']."</a></td>";
												echo "<td>".$barang['desc_barang']."</td>";
												echo "<td>".$barang['status_barang']."</td>";
												if($barang['id_user'] == $_SESSION['id']){
												?>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">
															<a href="edit.php?id=<?php echo $barang['id_barang']?>" class="dropdown-item" onclick="document.getElementById('id01').style.display='block'" ><i class="icon-pencil"></i> Edit Post</a>
															<a href="delete.php?id=<?php echo $barang['id_barang'] ?>" class="dropdown-item"><i class="icon-cross2"></i> Delete Post</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
												<?php
												}
											}
											?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- /marketing campaigns -->
					</div>
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


	<!-- Footer -->
	<div class="navbar navbar-expand-lg navbar-light">
		<div class="text-center d-lg-none w-100">
			<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
				<i class="icon-unfold mr-2"></i>
				Footer
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-footer">
			<span class="navbar-text">
				&copy; 2019. <a href="#">Justtip</a> by <a href="#" target="_blank">Pentol Team</a>
			</span>
		</div>
	</div>
	<!-- /footer -->
		
	<script> 

		var modal = document.getElementById('id01'); 
		
		
		window.onclick = function(event) { 
			if (event.target == modal) { 
				modal.style.display = "none"; 
			} 
		} 
	</script>

</body>
</html>
