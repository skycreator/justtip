<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Justtip</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="global_assets/js/main/jquery.min.js"></script>
	<script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="global_assets/js/plugins/ui/slinky.min.js"></script>
	<script src="global_assets/js/plugins/ui/fab.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script src="assets/js/app.js"></script>
	<script src="global_assets/js/demo_pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Page header -->
	<div class="page-header page-header-dark">

		<!-- Main navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-transparent">
		
			<div class="d-md-none">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
					<i class="icon-tree5"></i>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="navbar-mobile">
				<ul class="navbar-nav ml-md-auto">

					<li class="nav-item dropdown dropdown-user">
						<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
							<img src="global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" alt="">
							<span>Welcome, <?= $_SESSION['nama_depan'] ?></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right">
							<a href="profile.php" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
							<div class="dropdown-divider"></div>
							<a href="signout.php" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<!-- /main navbar -->


		<!-- Page header content -->
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4>Profile</h4>
				<a href="#" class="header-elements-toggle text-white d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none bg-transparent py-0 border-0 mb-3 mb-md-0">
				<form action="#">
					<div class="form-group form-group-feedback form-group-feedback-right">
						<input type="search" class="form-control bg-light-alpha border-transparent wmin-md-200" placeholder="Search">
						<div class="form-control-feedback">
							<i class="icon-search4 font-size-sm"></i>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- /page header content -->


		<!-- Secondary navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-top-0">
			<div class="d-md-none w-100">
				<button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse" data-target="#navbar-navigation">
					<i class="icon-menu-open mr-2"></i>
					Main navigation
				</button>
			</div>

			<div class="navbar-collapse collapse" id="navbar-navigation">
				<ul class="navbar-nav navbar-nav-highlight">
					<li class="nav-item">
						<a href="index.php" class="navbar-nav-link">
							<i class="icon-home4 mr-2"></i>
							Go to Home
						</a>
					</li>		
				</ul>
			</div>
		</div>
		<!-- /secondary navbar -->

	</div>
	<!-- /page header -->
		

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Content area -->
			<div class="content">

				<!-- Inner container -->
				<div class="d-md-flex align-items-md-start">

					<!-- Left sidebar component -->
					<div class="sidebar sidebar-light sidebar-component sidebar-component-left wmin-300 sidebar-expand-md">

						<!-- Sidebar content -->
						<div class="sidebar-content">

							<!-- Navigation -->
							<div class="card">
								<div class="card-body bg-indigo-400 text-center card-img-top" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
									<div class="card-img-actions d-inline-block mb-3">
										<img class="img-fluid rounded-circle" src="global_assets/images/placeholders/placeholder.jpg" width="170" height="170" alt="">
										<div class="card-img-actions-overlay rounded-circle">
											<a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
												<i class="icon-plus3" method="POST" enctype="multipart/formdata">
												</i>
											</a>											
										</div>
									</div>

						    		<h6 class="font-weight-semibold mb-0"><?= $_SESSION['nama_depan']." ".$_SESSION['nama_belakang']?></h6>
						    		<!-- <span class="d-block opacity-75"></span> -->
						    	</div>

								<div class="card-body p-0">
									<ul class="nav nav-sidebar mb-2">
										<li class="nav-item-header">Navigation</li>
										<li class="nav-item-divider"></li>
<!-- 										<li class="nav-item">
											<a href="signout.php" class="nav-link" data-toggle="tab">
												<i class="icon-switch2"></i>
												Logout
											</a>
										</li>
 -->									</ul>
								</div>
							</div>
							<!-- /navigation -->

						</div>
						<!-- /sidebar content -->

					</div>
					<!-- /left sidebar component -->


					<!-- Right content -->
					<div class="tab-content w-100 overflow-auto">
						<div class="tab-pane fade active show" id="profile">

							<!-- Profile info -->
							<div class="card">
								<div class="card-header header-elements-inline">
									<h5 class="card-title">Profile information</h5>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									<form action="usercrud.php" method="POST">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Nama Depan</label>
													<input type="text" value="<?= $_SESSION['nama_depan']?>" name="nama_depan" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Nama Belakang</label>
													<input type="text" value="<?=$_SESSION['nama_belakang']?>" name="nama_belakang" class="form-control">
												</div>
											</div>
																							
											<span class="form-text text-muted">* Nama maks. 30 karakter</span>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<label>Alamat</label>
													<input type="text" value="<?=$_SESSION['alamat']?>" name="alamat" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-4">
													<label>Kota</label>
													<input type="text" value="<?=$_SESSION['kota']?>" name="kota" class="form-control">
												</div>
												<div class="col-md-4">
													<label>Provinsi</label>
													<input type="text" value="<?=$_SESSION['provinsi']?>" name="provinsi" class="form-control">
												</div>
												<div class="col-md-4">
													<label>Kode Postal</label>
													<input type="text" value="<?=$_SESSION['kode_pos']?>" name="kode_postal" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Nomor Handphone</label>
													<input type="text" value="<?=$_SESSION['no_hp']?>" name="no_hp" class="form-control">
													<span class="form-text text-muted">Contoh : +62-812-3456-7890</span>
												</div>
												
											</div>
										</div>

<!-- 				                        <div class="form-group">
				                        	<div class="row">
												<div class="col-md-6">
													<label>Upload profile image</label>
				                                    <input type="file" class="form-input-styled" data-fouc>
				                                    <span class="form-text text-muted">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
												</div>
				                        	</div>
				                        </div>
 -->
				                        <div class="text-right">
				                        	<button type="submit" name="update" class="btn btn-primary">Save changes</button>
				                        </div>
									</form>
								</div>
							</div>
							<!-- /profile info -->


							<!-- Account settings -->
							<div class="card">  
								<div class="card-header header-elements-inline">
									<h5 class="card-title">Account settings</h5>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									<form action="usercrud.php">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Email</label>
													<input type="text" value="<?=$_SESSION['email']?>" name="email" readonly="readonly" class="form-control">
												</div>

												<div class="col-md-6">
													<label>Password saat ini</label>
													<input type="password" value="password" readonly="readonly" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Password baru</label>
													<input type="password" placeholder="Enter new password" name="password" class="form-control">
												</div>

												<div class="col-md-6">
													<label>Konfirmasi password baru</label>
													<input type="password" placeholder="Repeat new password" class="form-control">
												</div>
											</div>
										</div>

										

												
				                        <div class="text-right">
				                        	<button type="submit" class="btn btn-primary">Save changes</button>
				                        </div>
			                        </form>
								</div>
							</div>
							<!-- /account settings -->


				    	
				    	</div>
					</div>
					<!-- /right content -->

				</div>
				<!-- /inner container -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


	<!-- Footer -->
	<div class="navbar navbar-expand-lg navbar-light">
		<div class="text-center d-lg-none w-100">
			<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
				<i class="icon-unfold mr-2"></i>
				Footer
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-footer">
			<span class="navbar-text">
				&copy; 2019. <a href="#">Justtip</a> by <a href="#" target="_blank">Pentol Team</a>
			</span>
		</div>
	</div>
	<!-- /footer -->
		
</body>
</html>
